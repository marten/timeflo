# TimeFlo
Copyright &copy; 2021 *Marten Sova*

TimeFlo is an implementation of a
[Pomodoro&reg;](https://en.wikipedia.org/wiki/Pomodoro_Technique)-like
timer for breaking out of flow state.

## Status and Roadmap

First tested build is complete. See *Known Issues* and *Wishlist* below
for future plans.

* [x] Requirements complete.
* [x] Project plan complete.
* [x] Design complete.
* [x] Implementation complete.
* [x] Validation complete.

## Build and Run

*TimeFlo_MacOS_app.zip* contains the runnable app.

**Note** Depending on your Mac's security settings, you may
have to right-click the app icon, then cmd-click "Open."
This is because I am not an official verified Apple developer.

Alternately, you can open the Xcode project and
compile it yourself by choosing *Product/Run* from the menubar.
The Xcode project folder is *TimeFlo_MacOS/*

## Development Docs

Development documentation is available for TimeFlo, including:

* [Requirements Specification](docs/requirements.md)
* [Project Plan](docs/plan.md)
* [Design Doc](docs/design.md)
* [V&amp;V Report](docs/vnv.md)
* [Attribution](docs/attribution.md)

## Known Issues
* Notifications don't get sent if TimeFlo is the foreground window.
* "Pause" button doesn't change label to "Resume" while in the pause state.


## Wishlist
*Features that would be nice but haven't been implemented yet, because they weren't required.*

* Toggle alert audio
* Customizable work/break durations
* Menu bar indicator: either live timer or work/break symbol
* Option to continue the sequence by clicking a button on the notification
* App Icon
* Auto mode: automatically start next timer sequence instead of manually clicking "start work" and "start break"

# The TimeFlo Project: Requirements Document
*Marten Sova 2021*

## Introduction

This document contains the requirements for the development of the TimeFlo productivity timer application. The product, design, functionality, and use cases are described.

### Purpose and Scope

The design characteristics enumerated in this document are the core set of requirements initially agreed upon by the developer and client, which must be fulfilled or otherwise addressed.

### Target Audience

Developer and client.

### Terms and Definitions

None

## Product Overview

TimeFlo is a productivity timer app for MacOS, created as a school project. The timer should run in the background, interrupting the user periodically (20 min intervals), prompting them to take a 5 minute break

### Users and Stakeholders

#### Stakeholder 1: Marten Sova

Currently the sole developer of TimeFlo. Responsible for design, development, testing, and maintenance of the software.

#### Stakeholder 2: Bart Massey

Professor who assigned the TimeFlo project. Reflects the role of the client to some extent.

### Use cases

*Use cases describe the types of situations we expect this software to be primarily used in.*

#### Use Case 1: Computer worker

Human worker whose job requires them to be actively using their Mac computer for extended periods of time.

#### Use Case 2: General desk job

Human worker whose job requires them to be **near** a Mac computer for extended periods of time.

## Functional Requirements

*Specifies major core functionalities of the TimeFlo app. These are explicitly required for the app to function as designed.*

### 1. Timing system schedule

The timing system will operate on a schedule based on two constant time values, work time (20 min) and break time (5 min).  
Work periods are separated by break periods.  
When the work and break periods end, the user should be notified (3).

### 2. Interface

TimeFlo will have a GUI. The remaining time countdown should be displayed in the application.  
There should be clickable items for the following:  
* [Start] start default TimeFlo sequence of 20min work, 5min break, repeating.  
* Pause/Resume] pause the current sequence until manually resumed.  
* [Stop] stop the timer sequence entirely.  
* [Skip] skip to the end of the current timer  


### 3. Alerts
When work and break periods end, the user must be notified.  
Visual notification: Must be noticeable enough to break the user out of flow state.  
Audio alert: Should be toggle-able. Workers who aren't actively using their computer may need a louder sound.  
	*\*or maybe everyone should just get the audio alerts*


## Extra-functional Requirements

*Constraints on the system, development process, standards, etc.*

### 1. Target operating system
TimeFlo must run on the current Mac operating system. At the time of writing, version 11.6.  
*Reason: I am writing the program for myself and I use a Mac.*
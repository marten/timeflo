# The TimeFlo Project: Design
*Marten Sova 2021*

## Attribution
### Helpful articles and projects I've referenced while building TimeFlo

https://www.hackingwithswift.com/books/ios-swiftui/scheduling-local-notifications

https://www.hackingwithswift.com/quick-start/swiftui/how-to-run-code-when-your-app-launches

https://learnappmaking.com/timer-swift-how-to/

https://learnappmaking.com/creating-a-simple-ios-game-with-swift-in-xcode/#coding-the-countdown-timer

https://www.donnywals.com/publishing-property-changes-in-combine/

https://www.hackingwithswift.com/read/2/5/from-outlets-to-actions-creating-an-ibaction

https://medium.com/ivymobility-developers/unit-test-in-ios-1cc224aa35c3
# The TimeFlo Project: Design
*Marten Sova 2021*

## Introduction

This document describes the high level design of the TimeFlo app. The architecture is subdivided into simple modules.

## Architecture

### Module: UI view
Contains buttons to start, stop, and pause the timer sequence, and a toggle for audio alerts.

### Module: Start button
Initiates a new timer sequence, starting with the work period.

### Class: Sequence
Controller to set timers and display notifications at the appropriate times.

### Module: Pause/Resume button
Pause the current timer until clicked again.

### Module: Stop button
End the current sequence.

### Module: Skip button
Skip to the end of the active timer.

### Module: Timer
Initiate a timer, and return when its reached the requested duration.  
Input: timer duration

### Module: Notifier
Display a visual notification, and an audio notification if enabled.  
Input: String e.g., *"break is over"* or *"back to work!"*


## Simplified Control Flow
Input: Click start button  
Wait 20 min  
Notification: "Time to stop working! [Start] 5 min break]"  
Input: Click notification  
Wait 5 min  
Notification: "Back to work! [Start] 20 min work session"  
Input: Click notification  
Wait 20 min  
.....  
etc


# The TimeFlo Project: Validation and Verification
*Marten Sova 2021*

## Introduction

Welcome to the testing documentation for the TimeFlo app,
a productivity timer designed for breaking you out of
"flow state"

### Purpose and Scope

This document specifies the requirements testers should check for
while developing the TimeFlo application.

### Target Audience

Internal testing team

### Terms and Definitions

V&amp;V: Verification &amp; Validation

## V&amp;V Plan Description

*V&V is an ongoing process. Some functionality is
automatically tested, but much isn't.  
Just because the
tests pass, doesn't mean the application is necessarily
working correctly, or more importantly, working as the user
believes it should.*

## Test Plan Description

Testing is focused on manual QA and code inspection, with unit tests where applicable.
This document elaborates on what exactly those entail.

### Scope Of Testing

This is a simple GUI app with very simple functionality.
Unit tests are used minimally because there are not
any remotely complex calculations or operations being
carried out. There are also no user input text fields or
user configurable variables.  
Instead, testing will focus mainly on QA testing and code inspection.  
There are some basic performance tests as well.  
UI elements will not be tested automatically because it is difficult
to automate, and I don't have the time or resources to do so

### Testing Schedule

Nov. 7: Code Review, Unit tests  
Nov. 8: QA Testing, System Test

### Release Criteria

* No known crashes
* Startup time under 2 seconds

## Unit Testing

*These are tests for specific functions and functionalities, tested in a vacuum.*

- **testFormatTime**: correctly converts an internal timer value (in seconds)
to the displayed format (MM:SS)

TimeFlo_MacOSTests/

### Strategy

This project is built on Apple's MacOS development framework, all of which is outside the scope of regular testing for this project.  
Currently, the scale of this app is small enough that there are so few modules, which do not interact with each other, that integration testing won't be applicable. If the project grows to the point of having a larger number of modules and unit tests, that will change.

### Test Cases

* TF_01 - QA for notification system

## System Testing

*System testing normally involves running all unit tests. 
Since we don't use many unit tests for this project, the system test is
mostly a basic performance test for startup time, etc.*

### Test Cases

Test case id: TF_01  
Unit to test: Notifications  
Assumptions  
Test data: Set WORK_TIME and BREAK_TIME to 5 & 3 seconds  
Steps to be executed

    1. Press Start work/break  
    
    2. Wait for timer to reach zero  
    
Expected Result: Notification pop-up displayed and alert sound played.  
Actual result  
Pass/Fail  
Comments  

## Inspection

Every new addition to the project should be reviewed by another developer.  
Seeing as I'm the only developer, I will use the strategy of explaining
the code to my cat or roommate.  
Formatting choices should largely adhere to Apple's recommendations.

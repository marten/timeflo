# The TimeFlo Project: Project Plan
*Marten Sova 2021*

## Resources

This project will be developed on and for MacOS 11.6.  
The development environment is Apple's Xcode system,
using Swift and SwiftUI. Xcode includes building and
testing tools.

## Work Breakdown

### UI Mockup
Hand drawn or photoshop ok

### Non-functional UI
Build GUI in Xcode, buttons don't have to do anything yet

### Notification module
Send visual and audio alerts.  
Input: message string

### Timer module
Display formatted countdown-style timer

### Controller class
Constant break and work durations  
methods for start/stop/pause  

## Schedule

| Date  |  Milestone |
|---|---|
| Nov 5  | Design mockup  |
| Nov 5  | Non-functional GUI  |
| Nov 7  | Alpha build  |
| Nov 8  | Tested build  |

## Milestones and Deliverables

*Major development milestones that will concern management and the client.*

### 1. Deliverable: Design mockup and docs ready for approval

Image representation of app user interface for approval by client.    
Design and planning documents for internal use.

### 2. Internal: Non-functional GUI build
Recreate the user interface in an Xcode project, without implementing all
the backend yet 

### 3. Internal: Alpha build
App appears to function correctly and meet all user requirements.

### 4. Deliverable: Tested build for approval  
App passes testing and QA. Figure out deployment plan to send client
the runnable app    
Should prepare a summary of any significant changes from the initial
agreed upon design   

### 5. Deliverable: FiNaL bUiLd based on client feedback
This may involve multiple rounds of feedback, changes,  and subsequent 
re-testing

//
//  ViewController.swift
//  TimeFlo_MacOS
//
//  Created by Marten Sova on 11/6/21.
//

import Cocoa
//import SwiftUI
import AppKit
import SwiftUI


public var BREAK_TIME = 300 //seconds
public var WORK_TIME = 1200 //seconds
public var END_OF_BREAK_MSG = "Back to work! 🤑"
public var END_OF_WORK_MSG = "Time to take a break 🥳"

class ViewController: NSViewController {
    @IBOutlet weak var timeLabel:NSTextField?
    @IBOutlet weak var pauseOutlet: NSButtonCell!
    @State var working = true
    
    var timer:Timer?
    var seconds = 5
    var paused = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        timeLabel?.stringValue = "00:00"
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }
    
    @IBAction func startPress(_ sender: Any) {//start break period
        paused = false
        pauseOutlet.state = NSControl.StateValue.off
        working = false
        startTimer(durationSeconds:BREAK_TIME)
    }
    @IBAction func startWorkPress(_ sender: Any) {
        paused = false
        pauseOutlet.state = NSControl.StateValue.off
        working = true
        startTimer(durationSeconds:WORK_TIME)
    }
    
    @IBAction func pausePress(_ sender: Any) {
        //TimerNotify(NotificationMessage: "Test Notification message text")
        paused = !paused
    }
    
    @IBAction func skipPress(_ sender: Any) {
        paused = false
        pauseOutlet.state = NSControl.StateValue.off
        seconds = 1
    }
    
    func startTimer(durationSeconds:Int) {
        seconds = durationSeconds
        timer?.invalidate()
        timeLabel?.stringValue = formatTime(durationSeconds:seconds)
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerStep), userInfo: nil, repeats: true)
    }
    
    @objc func timerStep() {
        if !paused {
            seconds -= 1
        }
        
        let min = (seconds / 60) % 60
        let sec = seconds % 60
        timeLabel?.stringValue = String(format: "%02d", min) + ":" + String(format: "%02d", sec)
        
        if seconds <= 0 {
            timer?.invalidate()
            timer = nil
            
            if working {
                TimerNotify(NotificationMessage: END_OF_WORK_MSG)
            }
            else {
                TimerNotify(NotificationMessage: END_OF_BREAK_MSG)
            }
        }
    }
}

func formatTime(durationSeconds:Int)->String { //seconds->MM:SS
    let min = (durationSeconds / 60) % 60
    let sec = durationSeconds % 60
    return String(format: "%02d", min) + ":" + String(format: "%02d", sec)
}

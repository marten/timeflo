//
//  notification.swift
//  TimeFlo_MacOS
//
//  Created by Marten Sova on 11/5/21.
//

import Foundation
import SwiftUI
import UserNotifications
//import AVFoundation

func RequestNotifications(){
    UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { success, error in
        if success {
            print("Notifications are enabled!")
        } else if let error = error {
            print(error.localizedDescription)
        }
    }
}

//note: push notifications only get sent if the app isn't in foreground
func TimerNotify(NotificationMessage:String){
    let content = UNMutableNotificationContent()
    content.title = NotificationMessage
    //content.subtitle = "subtitle"
    content.sound = UNNotificationSound.default

    // show this notification .1 seconds from now
    let trigger = UNTimeIntervalNotificationTrigger(timeInterval: TimeInterval(0.1), repeats: false)

    // choose a random identifier
    let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
    
    //AudioServicesPlayAlertSound(1013) //idk why this doesn't play a sound
    
    // add our notification request
    let notificationCenter = UNUserNotificationCenter.current()
    notificationCenter.add(request) { (error) in
       if error != nil {
          // Handle any errors.
           print("error sending notification")
       }
    }
}

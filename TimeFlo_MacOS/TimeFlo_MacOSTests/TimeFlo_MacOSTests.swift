//
//  TimeFlo_MacOSTests.swift
//  TimeFlo_MacOSTests
//
//  Created by Marten Sova on 11/6/21.
//

import XCTest
@testable import TimeFlo_MacOS

class TimeFlo_MacOSTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testformatTime() throws {
        var formatted:String
        let expected = "02:03"
        formatted = formatTime(durationSeconds:123)
        XCTAssertEqual(formatted, expected)
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
